# Vorlesungsmitschriften / Lecture Notes

Dies sind meine Vorlesungsmitschriften von verschiedenen Vorlesungen. Es wird empfohlen die Dateien in einem externen Programm, welches GitHub Flavored Markdown unterstützt, (z.B. Visual Studio Code) zu öffnen, da Formeln und Tabellen sonst möglicherweise nicht korrekt dargestellt werden

These are my various lecture notes. It is recommended that you open these files in a programm that supports GitHub flavoured Markdown (e.g. Visual Studio Code) otherwise formulas and tables might not be displayed correctly.
