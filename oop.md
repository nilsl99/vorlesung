# Object Oriented Programming

---
*18.04.2023 Lecture `I`*

## Administrativa
Staff:
+ Wolfgang Mulzer mulzer@inf.fu-berlin.de T9/112

There will be special lectures, designed for student who are new to programming. The next three lectures will be Python courses.

Criterea:
+ regular participation
+ active participation
+ exam

The homework will be released on Monday and you have two weeks to hand it in. The homework consists of three tasks worth 10 points each. You will need at least 60% of the points to get the active participation. Only two tasks will be corrected/marked. We can expect 10 homework sheets, so you will need 120 points. You will also have to present a solution to an exercise in the tutorial.

There will be an exam with a duration of 120 minutes. It takes place on July 27th 2023 between 10am and 12pm. The second exam will be on October 10th 2023 between 10am and 12pm. You can bring one handwritten DIN A4 paper to the exam. The exams take place in lecture halls 1a/b in the main building.

## Imperative Programming (Python)

Imperative programming is a programming paradigm. A programming paradigm is the key thought when designing a programming language. There are two main programming paradigms:
+ Declarative programming:
  + explain what the program should do
  + abstract, mathematical
  + functional programming (haskell)
  + logic programming (prolog)
  + request language (SQL)
+ Imperative programming:
  + list of instructions/steps
  + Python, Java, C, C++, Pascal, Ruby, Rust, Assembler, etc.
  + is oriented on existing hardware (von Neumann architecture)

---
*03.05.2023 Vorlesung `VI`*

Recap from the last lecture:
+ Pyton containers
  + Sets `s = {1, 3}`
  + Dictionaries `d = {"a": 10, 2: "dict"}`


## Functions, Recursion and Iteration

### Functions

In Python we can define functions the following way
```python
def name(arg1, arg2):
    # block
    return
```

Functions combine a sequence of instructions and give them a name. With the help of functions the programm can be structured better (procedual programming). Functions have different names in different programming languages and use cases:
+ function
+ procedure
+ method
+ subroutine
+ subprogramm

```python
def greating():
    print("Hello")
    print("Welcome to ALP2")

def square(x):
    return x*x
```

Functions can receive multiple arguments and return one parameter.

The arguments at the definition of the function are called formal arguments/paramters. The arguments when the function is called, are called actual arguments/parameters. When a function is called, the formal argmument get bound to the actual arguments. In Python this works according to the typical "Python rules". This can lead to side effects.

The bond between formal and actual parameters is handled differently in different programming languages. There are three different calling conventions:
+ call by value
+ call by reference
+ call by name

### Scope
Variables that are defined inside a function, are only valid inside that function. You cannot access them from the outside (local variable). It's possible to have covering of variables. With the keyword `global` you can access global variables from inside a function. This can lead to side effects and should be used sparingly.

### Recursion and Iteration

With recursion functions can call themself. This usually leads to simple solution of problems. This is not very efficient in imperative programming languages, because function calls are "expensive". One should use iterative solution whenever possible.

---
*09.05.2023 Lecutre `VII`*

Examples for algorithms that compute the greates common divisor.

```python
# recursive
def gcd(a, b):
    if a < b:
        a, b = b, a
    c = a - b
    if c == b:
        return c
    else:
        return gcd(b, c)
```

```python
# iterative
def gcd(a, b):
    while not a == b:
        if a > b:
            a, b = b, a-b
        else:
            a, b = b-a, a
    return a
```

```python
# improved iterative
def gcd(a, b):
    while b > 0:
        a, b = b, a%b
    return a
```

The last algorithm only takes $\log(a) + \log(b)$ steps to finish (which is proportional to the amount of digits of $a$ and $b$).

---
*10.05.2023 Lecture `VIII`*
#### The Mensa Crad Problem

Let $B$ be the budget on a mensa card you found and $p_i \in P$ the prices of a meal in the mensa.

Is it possible to spend the whole money on the mensa card?

$$\sum_{p_i \in \mathbb{P} \subset P} p_i = B$$

This is an recursive algorithm for this problem:
```python
def mensa_card(B, P):
    if P == []:
        return B == 0
    if P[0] <= B:
        if mensa_card(B - P[0], P[1:]):
            return True
    return mensa_card(B, P[1:])
```
One can see, that there are many function calls with the same argument. We can improve the runtime if we cache the results in a dictionary:
```python
def mensa_card2(B, P, results = {}):
    if (B, len(P)) in results:
        return results[(B, len(P))]
    if P == []:
        r = (B == 0)
        results[(B, 0)] = r
        return r
    if P[0] <= B:
        if mensakarte2(B - P[0], P[1:], results):
            results[(B, len(P))] = True
            return True
    r = mensakarte2(B, P[1:], results)
    results[(B, len(P))] = r
    return r
```

Another solution is to use dynamic programming with an iterative algorithm:
```python
def mensa_card3(B, P):
    n = len(P)
    a = [([False] * (B + 1)) for _ in range (0, n + 1)]
    a[n][0] = True
    for i in range(n - 1, -1, -1):
        for b in range(0, B + 1):
            if P[i] <= b and a[i + 1][b - P[i]]:
                a[i][b] = True
            else:
                a[i][b] = a[i + 1][b]
    return a[0][B]
```

---
*16.05.2023 Lecture `IX`*

Most recursive algorithms can be turned into iterative algorithms using a matrix of the solutions for "smaller" arguments. This method is called dynamic programming.

## Sorting and Searching

### Sorting

Let $X = x_0, x_1, \ldots, x_{n-1}$ be a list of elements $x_i \in \mathbb{X}$. $\mathbb{X}$ is a total ordered set.

We want to calculate a permutation $\Pi: [0, \ldots, n-1] \to [0, \ldots, n-1]$ that satisfies the following condition:
$$x_{\Pi(0)} \le x_{\Pi(1)} \le x_{\Pi(2)} \le \ldots \le x_{\Pi(n-2)} \le x_{\Pi(n-1)}$$

Why do we want to sort something?
+ To make the search faster (binary search)
+ To solver another problem
+ We're interested in the order of the list (price comparison, search results).

---
*17.05.2023 Lecture `X`*

#### Binary Search
If we're given a sorted list we can use binary search to find elements efficiently ($\mathcal{O}(\log(n))$).

```python
def binary_search(a, k, l=0, r=None):
    if r is None:
        r = len(a) - 1
    m = l + (r - l)//2
    if a[m] > k:
      if l < m:
          return binary_search(a, k, l, m-1)
      else:
          return None
    if a[m] < k:
      if r > m:
          return binary_search(a, k, m+1, r)
      else:
          return None
    return m
```

#### Merge Sort
To sort a list we can divide the list into two sublists, sort each sublist independendly and then merge both sublists back together.

```python
def merge_sort(a):
    n = len(a)
    if n <= 1:
        return
    m = n // 2
    left = a[:m]
    right = a[m:]

    msort(left)
    msort(right)

    i,l,r =  0,0,0
    while i < n:
     if l == len(left):
         a[i] = right[r]
         r = r+1
     elif r == len(right):
         a[i] = left[l]
         l = l + 1
     elif left[l] <= right[r]:
          a[i] = left[l]
          l = l + 1
     else:
          a[i] = right[r]
          r = r + 1
     i = i + 1
```

---
*23.05.2023 Lecture `XI`*

There are many different sorting algorithms:
+ Selection Sort
+ Insertion Sort
+ Merge Sort
+ Quicksort

Merge Sort uses the divide and conquer approach to sort a list very fast, but it uses additional lists which leads to a larger memory footprint and a lot of copying of list elements.

In conrast to Merge Sort, Quicksort first sorts the whole list and the divides it into smaller lists. The algorithm picks a (random) element called pivot element. It then puts all elements smaller than the pivot element to the left and all elements larger than the pivot element to the right. It then sorts the left and right side recursively.

```python
def quicksort(a, left=0, right=None):
    if right is None:
        right = len(a) - 1

    if left >= right:
        return

    c = (left+right)//2
    a[c], a[right] = a[right], a[c]

    pivot_index = right

    l = left
    for i in range(left, right):
        if a[i] < a[pivot_index]:
            a[l], a[i] = a[i], a[l]
            l += 1
    a[l], a[pivot_index] = a[pivot_index], a[l]

    quicksort(a, left, l-1)
    quicksort(a, l+1, right)
```

The speed of quicksort depends on the choice of the pivot element. If we choose the pivot element randomly the list will be split equally most of the time.

If an algorithm uses randomness, is deterministic, but the runtime depends on the randomness, it's called Las-Vegas-Algorithm: "Las Vegas never cheats". This is in contrast to Monte-Carlo-Algorithms, where the result can be wrong but this is very unlikely.

---
*24.05.2023 Lecture `XII`*

#### Data Structures
A data structureis a method to represent and structure data inside the computer, to provide certain functions.

#### Heap Sort
The idea of Heap sort is to use a approapriate data structureto sort the list. We can use a priority queue, which has two methods:
+ `insert`: Insert an object into the correct slot
+ `delete_max`: Delete the object with the highest priority and returns it

These methods can be implemented in many different ways. Heap Sort uses a very efficient implementationn, namely a binary max-heap.
A binary max-heap is an almost perfect binary tree where the maximum is inside the root. In a binary tree knot can have at most two children. In an almost perfect tree all layers except the last layer are completely filled and the last layer is filled from the left side. All childrens are smaller or equal to its parent in a max-heap.

Binary Heaps can be implemented very efficiently as an array. The children and parents can be found by these simple equations:
```python
def p(i): # parent
    return (i - 1) // 2

def l(i): # left child
    return 2 * i + 1

def r(i): # right child
    return 2 * i + 2
```

---
*30.05.2023 Lecture `XIII`*

```python
def bubble_up(heap, i):
    while i > 0 and heap[p(i)] < heap[i]:
        heap[i], heap[p(i)] = heap[p(i)], heap[i]
        i = p(i)
```

```python
def bubble_down(heap, size, i=0):
    while (l(i) < size and heap[l(i)] > heap[i]) or \
          (r(i) < size and heap[r(i)] > heap[i]):
        if r(i) < size and heap[r(i)] > heap[l(i)]:
            heap[i], heap[r(i)] = heap[r(i)], heap[i]
            i = r(i)
        else:
            heap[i], heap[l(i)] = heap[l(i)], heap[i]
            i = l(i)
```

```python
def heap_sort(a):
    # Construct the heap
    for i in range(1, len(a))
        bubble_up(a, i)

    # Construct the sorted list
    for i in range(len(a) - 1, 0, -1):
        a[0], a[i] = a[i], a[0]
        bubble_down(a, i)
```

The algorithm covered in thsi course so far, are comparison based sorting algorithms. There are other sorting algorithms that can only be used specific data (e.g integers) that are faster.

### Analysis of Algorithms

We want to know: How good is an algorithm? There are many possible criteria which we can use to decide if an algorithm is good:
+ correctness
  + It exist a mathematical exact specification of the desired output and we garantee that the algorithm finds the right output for every possible input after a finite amount of steps. (Hoare logic)
+ efficiency
  + The complexity or the number of steps, the algorithm needs to find a solution/terminate
  + The amount of memory the algorithms needs.

---
*31.05.2023 Lecture `XIV`*

#### Abstract Machine Model

This is a mathematical idealized model of real hardware. The abstract machine model defines as set of primitive operations and the associated costs.

An algorithm consists of primitive operations. The runtime for a given input is the sum of all executed operations.

There are many different abstract machine models:
+ Touring Machine
  + single-tape
  + multi-tape
+ lambda calculus
+ register machine

A favoured machine model for sorting algorithmsis the comparison based machine. It offers the following primitive operations:
+ compare two elements of the input list (cost: 1)
+ swap two elements of the input list (cost: 0)
+ arbitrary operations that do not interact with the input list (cost: 0)

This means the runtime of a sorting algorithm is equal to the number of performed comparisons.

The next quetion is: For which inputs are we going to analyze the algorithm? \
Answer: For all possible inputs

All inputs have a length $n$. The input length $n$ is a whole number and a property of the problem. For example:
+ the length of the list in the sorting problem
+ the amount of digits of the numbers in the gcd problem
+ the amount of dishes and the number of digits of the budget in the mensa card problem

The runtime of the algorithm is a function:
$$T: \mathbb{N} \to \mathbb{N}$$

$T(n)$ is the maximum runtime for a given input length and all inputs.

This worst case analysis is pessimistic and potentially doesn't reflect the typical behaviour of the algorithm.

![Worst Case Runtime of Insertions Sort (Graph)](runtime.png)

The exact formula for $T(n)$ gets complicated quickly! Thats why we are focusing only on the dominant term in the formula. This is known as $\mathcal{O}$-notation

---
*06.06.2023 Lecture `XV`*

#### Asymptotical Analysis / $\mathcal{O}$-Notation

We focus on the basics and only calculate the leading term of $T$. This makes the runtime visualy pleasing and easy to compare between different algorithms/platforms.

Let $f, g: \mathbb{N} \to \mathbb{R}^+$ be functions.

$$ f \in \mathcal{O}(g) \Longleftrightarrow \exists c > 0: \exists n_0 \in \mathbb{N}: \forall n > n_0: f(n) \le c \cdot g(n) $$
$$ f \in \mathcal{\Omega}(g) \Longleftrightarrow \exists c > 0: \exists n_0 \in \mathbb{N}: \forall n > n_0: f(n) > c \cdot g(n) $$
$$ f \in \mathcal{\Theta}(g) \Longleftrightarrow f \in \mathcal{O}(g) \land f \in \mathcal{\Omega}(g) $$

We can now say: The runtime of Insertion Sort is $\Theta(n^2)$.

These are typical runtimes:
- $\mathcal{O}(1)$ constant runtime
  - array look up
- $\mathcal{O}(\log(n))$ logarithmic runtime
  - binary search
- $\mathcal{O}(\sqrt{n})$ sublinear runtime
  - search in an unsorted list with quantum computers
- $\mathcal{O}(n)$ linear runtime
  - search through all inputs
  - search in an unsorted list
- $\mathcal{O}(n \log(n))$ linearithmic runtime
  - comparison based sorting
- $\mathcal{O}(n^k)$ polynomial runtime
  - matrix multiplication
  - is considered efficient
- $\mathcal{O}(2^n)$ exponential runtime
  - go through a subsets of the input
  - is not efficient
- $\mathcal{O}(n!)$ factorial runtime
  - go through all permutations of the input
  - is very slow

---
*07.06.2023 Lecture `XVI`*

We now analyze the runtime of Merge Sort. With the assumption that $n$ is a power of 2, we can define the runtime with a recursive equation:
$$ T(1) = 0 $$
$$ T(n) = 2 \cdot T\left(\frac{n}{2}\right) + n $$

There many different ways to solve these equations for $T(n)$, but no standard way that works for all recursive equations. The solution to this is:

$$ T(n) = n \cdot \log_2(n) $$
$$ \implies T(n) \in \mathcal{O}(n \log(n)) $$

---
*13.06.2023 Lecture `XVII`*

#### Lower Bound for Comparison Based Sorting

Question: Which is the fastest possible sorting algorithm.

Question: Does a function $U: \mathbb{N} \to \mathbb{N}$ exists so that for every comparsion based sorting algorithm $A$ and every inputsize $n$ there exist an input $I$ so that $A$ needs a minimum of $U(n)$ comparisons to compute the sort order for $I$?

We know that $U(n) = 1$ is a answer to this question. A better answer is $U(n) = n - 1$ (Proof in the lecture is wrong. The correct proof will be send via email). The correct answer is $U(n) \in \Omega(n \log(n))$ (Proof in the lecture).

This is only true for comparison based algorithms. There exist other sorting algorithms, that use more information of the elements to be sorted. These alorithms can have a lower bound that $\Omega(n \log(n))$.

---
*14.06.2023 Lecture `XVIII`*

#### Programm Verification

Question: How can i know that my programm is correct?

Question: What does it mean for an programm to be correct?

Answer: It exists a precise specification which defines exactly what the programm should do. When there is a specification correct means that the programm meets the specification.

This leads to another question: Is it always possible to give an exact specification? The answer is: For simple programms we can give an exact specification, but in complex cases the specification has to be created gradually and might not represent every possible use case. One can however give specifications for small parts of the programm and verify their correctness.

There are many possibe ways to represent a specification, we concentrate on the following way. A specification consists of two parts:
- A Precondition
- A Postcondition

The statement of the specification is: If the system is in a state which is equal to the precondition then the programm will move the programm into a state which is equal to the postcondition in a finite amount of steps/time.

Example for a programm that calculates the squareroot of a number a:\
Precondition: `a >= 0` (`a` is a float) \
Postcondition: `b >= 0 and b*b <= 1.1*a and b*b >= 0.9*a` (`b` is a float)

The fact that a programm $S$ meets a specification is formalized with a so called programm formula:
$$\{P\} S \{Q\}$$

Where $P$ is the precondtion, $S$ is the programm (a set of instructions) and $Q$ is the postcondition.

This programm formula is valid if and only if for every state that is equal to $P$, $S$ is started and leads the state in a finite amount of steps/time into a state equal to $Q$.

Example:
$$\{x > 0\} x = x-1 \{x > -1\}$$
This is a valid programm formula

$$\{x > 0\} x = x - 10 \{x > 0\}$$
This is an invalid programm formula

Programm verification means: We show that a program formula is valid/correct.

This can be done with the help of Hoare logic. A logic consists of
+ Axioms:
  + Specific program formulas are valid.
+ Rules of derivation:
  + If a specific program formula is valid then I can conclude that other program formulas are also valid

---
[Lecture Evaluation](https://lehrevaluation.fu-berlin.de/productive/de/sl/Z05bc1AB09Cv)

---
*20.06.2023 Lecture `XIX`*

#### Hoare Logic

Rules of derivation can be written down in the following way:

$$\frac{\text{cond}_1, \text{cond}_2, \ldots, \text{cond}_k}{\text{derivation}}(\text{name})$$

Theseare the rules of derivation in the Hoare logic:
1. sequence rule:
   $$\frac{\{P\}S_1\{R\}, \{R\}S_2\{Q\}}{\{P\}S_1, S_2\{Q\}}(\text{seq})$$

2. if rule:
   $$\frac{\{P \land B\}S_1\{Q\}, \{P \land \bar{B}\}S_2\{Q\}}{\{P\} \:\text{if}\: B\!: S_1 \:\text{else}\: S_2 \{Q\}}(\text{if})$$

3. consequence rule
   $$\frac{P \implies P', \{P'\}S\{Q'\}, Q' \implies Q}{\{P\}S\{Q\}}(\text{cons})$$

4. while rule
   $$\frac{\{P\land B\} S \{P\}}{\{P\} \:\text{while}\: B\!: S \{P \land \bar{B}\}}(\text{while})$$
   The condition $P$ is called loop invariant and has to be true for every pass of the loop. It can be difficult to find a good loop invariant. We cannot use this rule to show that a while loop terminates.

This is the only Axiom in the Hoare logic:
1. assignment axiom
   $$\frac{}{\{Q\underbrace{[x/E]}_*\} x = E \{Q\}}(\text{assign})$$
   \* replace every occurence of $x$ in $Q$ with $E$.

---
*21.06.2023 Lecture `XX`*

## Java (pre-course)

Java is an imperative programming language just like Python. The concepts in Java are similar to the concepts in Python, but there are many details that are different between these two languages.

Python is an interpreted language. Java on the otherhand is a compiled language. Python is translated into machine code and executed by an interpreter (python). Java programs have to be translated/compiled into a (virtual) machine language first. The resulting programm (`.class` file) will be executed by a virtual machine (java).

```java
public class JavaExample {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
```
This code has to be in a file called `JavaExample.java`. We can now compile this program with
```cmd
javac JavaExample.java
```
and execute it afterwards with
```cmd
java JavaExample
```

Java is statically typed. This means that we have to define a type for each variable. The type of a variable cannot be changed. Priorto using a variable you have to declare that variable (and his type).

```java
public class Test {
    // This is a comment
    /*
        This is a multiline comment
    */
    public static void main(String[] args) {
        int i; // Declaration
        i = 5; // Definition/Assignement
        System.out.println(2+i); // Prints '7' to the console
    }
}
```

### Syntax

Java and Python programs consist of a list of instructions. Python is layout sensitive, but Java is not! In Python whitespaces and newlines play a critical role, but not in Java. Code blocks are combined using curly braces in Java. Every instruction has to end with a semicolon (`;`).

---
*27.06.2023 Lecture `XXI`*

#### Controll Structures

Java has these basic control structures:
+ `if` (and `else`)
+ `for` loop
+ `while` loop

```java
if (statement) {
    System.out.println("statement is true")
} else {
    System.out.println("statement is false")
}
```

```java
while (statement) {
    System.out.println("inside the while loop");
}
```

```java
do {
    System.out.println("inside the do-while loop");
} while (statement);
```

```java
for (int i = 0; i < 10; i++) {
    System.out.println("This loop runs 10 times");
}
```

A function has to define a return type and the types of all of its arguments.

```java
static void function(int argument) {
    argument++;
}
```

#### Data Types

There are primitive data types and objects. The following are primitive data types:
+ `byte, short, int, long` (whole numbers)
+ `float, double` (floating point numbers)
+ `char` (characters)
+ `boolean` (truth value)

A string can be created with double quotes (`"example string"`). It will create an object of the `java.lang.String` class.

There are many different combined data types like arrays. Arrays can only contain a single type of values and you cannot change the size of the array.

```java
int[] = our_array;
our_array = new int[10];
// or
our_array = {1, 2, 3, 4, 5};
```

---
*28.06.2023 Lecture `XXII`*

## Object-oriented Programming

Problem: How can I develop software on a large scale?

Goals:
+ controllability
+ clarity
+ maintainability
+ expandability
+ reusability
+ simple debugging/troubleshooting

The first step to achieve our goals is the structual and procedural programming (1970s, ALGOL). It only allows predefined simply nested control structures (loops, branches, blocks, no GOTOs). It lets us divide our program into subprogram, procedures, subroutine, functions for a better clarity and reusability.

The second step is modular programming. This includes information hiding. This lets us divide our program into independent, interchangable modules that cover different aspects of the functionality. A module defines an interface which gives access to the functionality from the outside. This interface is specified exactly and defines under what conditions different outputs are expected. The implementation is hidden from the outside and can be exchanged (as long as the interface/specification remains unchanged). This is supported in almost any programming language today (java packages, python modules/packages).

The third step is object-oriented programming. OOP combines data and function into classes and object. Objects are incarnations of classes. So a class is like a blueprint for an object. An object is a collection of data (attributes, properties) and methods (functions, which operate on the data of the object). The methods are the same for objects of the same class. An object has a state and an identity. The state is the values of all attributes. Objects with the same state have different identities. Objects help to better structure programms and to model real life entities inside the program. They should make code more clear and easier to understand.

---
*04.07.2023 Lecture `XXIII`*

#### `static`

The keyword `static` can be placed in front of methods. These methods can be called without creating an object of that class. Everything used inside a static methods has to be static as well.

`static` can also be placed in front of variables. These variables are shared between all object of this class and are called "class variables".

#### Subfolders

We have the following folder structure:
+ src
  + Main.java
  + Uni
    + Student.java

We then have to add this line to the top of the Student.java file:
```java
package Uni;
```
and this line to the Main.java file:
```java
import Uni.Student;
```

#### Access Modifiers

Access modifiers defines interfaces of class from the outside. There used to apply the information hiding principle. There are three access modifiers in Java:
+ `public`:
  + Everyone can access it
+ `private`:
  + Can only be accessed from inside that class
+ `protected`:
  + Can only be accessed from inside the same package

If we don't specify the accessability, it is `protected` by default.

#### Constructors

A constructor is a method, that is called when an object is created. It should be at the top of the class and looks like this:
```java
public <ClassName>() {
    // Constructor content
}
```

A constructor has no return type and has the same name as the class.

#### Local vs. Global Variables

Global variables are valid in the whole class. Local variables are only valid inside their code block. Local variables cover global variables. One can access the global variables with the keyword `this`:
```java
this.variables;
```

#### `final`

If we put `final` in front of a variable, this variable cannot be changed afterwards.

#### Exceptions

One can raise an exception. If a function can raise an exceptions it has to be declared in the function signature.

```java
public void function() throws Exception {
    raise new Exception("This is an exception");
}
```

We can catch an exception so the program doesn't crash in the following way:
```java
try {
    // An exception will be cathed inside this code block
} catch (Exception e) {
    // if we catch an exception, this code block will be executed
}
```

#### Enums

Enums create a new type that can only take one of the specified values.

```java
public enum compass_direction {NORTH, EAST, SOUTH, WEST};
```

---
*05.07.2023 Lecture `XXIV`*

### Inheritance

A class can inherit all attributes and methods of another class and is called subclass. The classfrom which the attributes and methods are inherited is called superclass. The subclass can add new attributes and methods or override methods.

```java
class Student extends Human {
    // class content
}
```

One can cast object to another type if both types are compatible. We can check if they are compatible with the keyword `isinstanceof`.

```java
for (Human h: list_of_humans) {
    if (h isinstanceof Student) {
        System.out.println(h + " is a Student");
    } else {
        System.out.println(h + " is not a Student");
    }
}
```

It is not possible to extend from multiple classes. One can use interfaces to define an interface which do not contain attributes or implementation of methods. Theyonlyprovide signatures of methods that have to be implemented by the subclass.

```java
public interface <InterfaceName>
```

Classes can now inherit method signatures from multiple interfaces

```java
public class <ClassName> implements <InterfaceName>
```

---
*11.07.2023 Lecture `XXV`*

### Abstract Data Types in Java

Abstract data types provide functionalities. Implementation and supported operation are split. There are different abstract data types, that differ in their functionality. We will cover these abstract data types:
+ Stack
+ Queue
+ Double Ended Queue (Deque)
+ Dictionary

#### Stack
A stack provides the following methods:
+ `push(x)`: adds `x` to the stack
+ `pop`: removes the last element and returns it
+ `isEmpty`: returns wether stack is empty or not

```java
public interface Stack<Type> {
    public void push(Type item);
    public Type pop();
    public boolean isEmpty();
}
```

```java
public class MyStack implements Stack<Type> {
    int top;
    A[] items;
    final int SIZE;

    public ArrayStack(int size) {
        this.SIZE = size;
        this.items = (A[]) new Object[size];
        this.top = 0;
    }

    public void push(A item) {
        if (top < SIZE) {
            items[top] = item;
            top++;
        }
    }

    public A pop() {
        if (!isEmpty()) {
            top--;
            return items[top];
        } else {
            return null;
        }
    }

    public boolean isEmpty() {
        return this.top == 0;
    }
}
```

---
*12.07.2023 Lecture `XXVI`*

This implementation of a Stack has a limited size. We can use a dynamic array to accomodate arbitrary sizes. This leads to uneven runtimesof the push/pop operation (the amortized costs are still constant $\mathcal{O}(1)$).

We can also use a linked list to implement a stack. Each element is an object that stores the value and the next Element in the stack.

```java
class StackElement {
        StackElement next_item;
        Object value;

        public StackElement(Object value, StackElement next_item) {
            this.next_item = next_item;
            this.value = value;
        }
    }
```

```java
public class ListStack implements Stack {
    StackElement top;

    public void push(Object item) {
        top = new StackElement(item, top);
    }

    public Object pop() {
        if (top != null) {
            Object item = top.value;
            top = top.next_item;
            return item;
        } else {
            return null;
        }
    }

    public boolean isEmpty() {
        return this.top == null;
    }
}
```

Note that we usually include the `StackElement` inside the `ListStack` class.

#### Queue

A queue is similar to a stack, but is a kind of FIFO (first in first out) storage. It provides the following operations:
+ `enqueue(x)`: Attach `x` to the end of the queue
+ `dequeue()`: Remove the first element of the queue and return it
+ `isEmpty()`: Return wether the queue is empty or not

```java
public interface Queue {
    public void enqueue(Object item);
    public Object dequeue();
    public boolean isEmpty();
}
```

```java
public class ListQueue implements Queue {
    Element front;
    Element back;

    public void enqueue(Object item) {
        Element new_item = new Element(item, null);
        if (back != null)
            back.next_item = new_item;
        back = new_item;
        if (front == null)
            front = back;
    }

    public Object dequeue() {
        if (front != null) {
            Object item = front.value;
            front = front.next_item;
            if (front == null)
                back == null;
            return item;
        } else
            return null;
    }

    public boolean isEmpty() {
        return this.front == null;
    }

    class Element {
        Element next_item;
        Object value;

        public Element(Object value, Element next_item) {
            this.next_item = next_item;
            this.value = value;
        }
    }
}
```

---
*18.07.2023 Lecture `XXVII`*

You can also implement a queue using an array and a ring buffer.

#### (Sorted) Dictionary

In Python dictionaries are included in the language definition, not so in Java. In Java we have use a module or implemented it ourself. A dictionary saves key-value-pairs and provides the following methods:
+ `put(key, value)`
+ `get(key)`
+ `remove(key)`
+ `isEmpty()`
+ `pred(key)`: returns the predeccessor to the given key
+ `succ(key)`: return the successor to the given key

You can find the source code online (FU Whiteboard).
