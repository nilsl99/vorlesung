# Statistical Physics

---
*Lecture `II`*

## Probability

### Discrete Probabilities
There are a finite number of discrete outcomes $m$.
$n(m)$ # times of outcome $m$ is found out of $N$ total.

Frequency $h(m) = \frac{n(m)}{N}$

Probability $p(m) = \lim_{N \rightarrow \infty} h(m)$

$$\begin{aligned}
p(m) &\ge 0 \\
\sum_{\{m\}} p(m) &= 1
\end{aligned}$$

If we have two events $m_1, m_2$ that are mutually exclusive.
$$\implies p(m_1 \lor m_2) = p(m_1) + p(m_2)$$

If two events $m_1,m_2$ that can happen at the same time, but are independent.
$$\implies p(m_1 \land m_2) = p(m_1) \cdot p(m_2)$$

The expactation value $\langle X \rangle$ of a Variable $X(m)$ is defined by
$$\langle X \rangle = \sum_{m} p(m) X(m)$$

The variance $\Delta X^2 = \langle (X - \langle X \rangle)\rangle = \langle X^2 + \langle X \rangle^2 - 2X\langle X \rangle\rangle = \langle X^2 \rangle - \langle X \rangle^2$

Deviation $\Delta X = \sqrt{\langle (X - \langle X \rangle)\rangle} = \sqrt{\langle X^2 \rangle - \langle X \rangle^2}$

$$\lim_{N \rightarrow \infty} \frac{\Delta X}{\langle X \rangle} \rightarrow 0 \quad \text{Thermolimit}$$

Random Walk: \
Binomial Distribution (and the limit $N \rightarrow \infty$)

---
*25.04.2023 Lecture `III`*

Binomial Distribution
$$ P_N(m) = \binom{N}{m} p^m (1-p)^{N-m}$$

In the the limit $N \to \infty$
$$P_N(m) \to \frac{1}{\sqrt{2\pi \Delta m^2}} e^{-\frac{(m-\langle m \rangle )^2}{2\Delta m^2}}$$

Now we look at the limit $N \to \infty$ with $p \to 0$ ($Np$ finite)
$$P_N(m) \to \frac{1}{m!}\lambda^m e^{-\lambda}$$

### Continuous Probabilities
variabe $x$ \
probability $p(x)$ \
normalization $1 = \int_\mathbb{R} p(x) \mathrm{d}x$ \
expactation value $\langle x \rangle = \int_\mathbb{R} x p(x) \mathrm{d}x$ \
moments $\langle x^m \rangle = \int_\mathbb{R} x^m p(x) \mathrm{d}x$

### Charactaristic Function
$$\begin{aligned}
G(k) &= \int_\mathbb{R} p(x) e^{-ikx} \mathrm{d}x = \langle e^{-ikx} \rangle \\
G(k) &= \sum_{n=0}^\infty \frac{(-ik)^n}{n!} \langle x^n \rangle \\
\ln[G(k)] &\to \text{CUMULANTS} \; \langle x^n \rangle_c = i^n \frac{\mathrm{d}^n}{\mathrm{d}k^n}\ln[G(k)] \\
\langle x \rangle_c &= \langle x \rangle \\
\langle x^2 \rangle_c &= \langle x^2 \rangle - \langle x \rangle ^2 \\
\langle x^3 \rangle_c &= \langle x^3 \rangle - 3 \langle x \rangle\langle x^2 \rangle + 2 \langle x \rangle^3
\end{aligned}$$

We now consider a gaussian distribution.
$$\langle x^3 \rangle_c = 0$$

### Multivariate Distribution
Marginalization
$$p(x_1) = \int\mathrm{d}x_2 \int\mathrm{d}x_3 \ldots p(x_1,x_2,x_3,\ldots)$$

Moments
$$\langle x_1^{n_1} x_2^{n_2} x_3^{n_3} \ldots \rangle$$

Covariance
$$\mathrm{cov}(x_i, x_j) = \langle x_i x_y \rangle - \langle x_i \rangle\langle x_j \rangle$$

---
*28.04.2023 Lecture `IV`*

### Central Limit Theorem

You're given a probability distribution $p(x)$ and you extract $n$ random variables.

$$ y = \frac{\sum_{i=1}^n x_i}{n}$$

$$W(y) = \int\mathrm{d}x_1 \int \mathrm{d}x_2 \ldots \int\mathrm{d}x_n p(x_1) p(x_2) \ldots p(x_n) \cdot \delta\left( y - \frac{\sum_{i=1}^n x_i}{n}\right)$$

Now we calculate the moments to characterize this distribution.

$$\begin{aligned}
    \langle y^m \rangle &= \int\mathrm{d}y W(y)y^m \\
    &= \int\mathrm{d}y \int\mathrm{d}x_1 \ldots\int\mathrm{d}x_n y^m p(x_1) \ldots p(x_n) \cdot \delta\left( y - \frac{\sum_{i=1}^n x_i}{n}\right) \\
    &= \int\mathrm{d}x_1 \ldots\int\mathrm{d}x_n \left(\frac{\sum_{i=1}^n}{n}\right)^m \cdot p(x_1) \ldots p(x_n) \\
    &= \left\langle \left(\frac{\sum_{i=1}^n x_i}{n}\right)^m \right\rangle
\end{aligned}$$

Now we want to calculate the cumulants.

$$\begin{aligned}
    G(k) &= \int\mathrm{d}y W(y) e^{-ikx} \\
    &= \int\mathrm{d}y \int\mathrm{d}x_1 \ldots \int\mathrm{d}x_n p(x_1) \ldots p(x_n) \cdot e^{-ikx} \cdot \delta\left( y - \frac{\sum_{i=1}^n x_i}{n}\right) \\
    &= \int\mathrm{d}x_1 \ldots \int\mathrm{d}x_n p(x_1) \ldots p(x_n) \cdot e^{-ik \frac{\sum_{i=1}^n x_i}{n}} \\
    &= \left[\int\mathrm{d}x \: e^{-ik\frac{x}{n}}\right]^n \\
    &= \left[g\left(\frac{k}{n}\right)\right]^n \\
    \ln\left(G(k)\right) &= n \ln\left(g\left(\frac{k}{n}\right)\right)
\end{aligned}$$

$$\begin{aligned}
    \langle y^m \rangle_c &= i^m \left.\frac{\mathrm{d}^m \ln(G(k))}{\mathrm{d}k^m} \right|_{k=0} \\
    &= i^m n \left. \frac{\mathrm{d}^m \ln \left(g\left(\frac
    {k}{n}\right)\right)}{\mathrm{d}k^m}\right|_{k=0} \\
    &= n i^m \frac{\mathrm{d}^m \ln(g(q) n^{-m})}{\mathrm{d}q^m} \\
    &= n^{1-m} \langle x^m \rangle_c
\end{aligned}$$

$$\begin{aligned}
    m\ddot{x} &= F(x, t) = \frac{-\partial U(x, t)}{\partial x} \\
    p &= m \dot{x} \\
    \dot{p} + \frac{\partial U(x, t)}{\partial x} &= 0 \\
    \frac{\mathrm{d}}{\mathrm{d}t} \frac{\partial T}{\partial \dot{x}} + \frac{\partial U}{\partial x} &= 0 \\
    L &= T - U \\
    \frac{\mathrm{d}}{\mathrm{d}t} \frac{\partial L}{\partial \dot{x}} - \frac{\partial U}{\partial x} &= 0
\end{aligned}$$

Action Functional
$$\begin{aligned}
    S &= \int_{t_0}^{t_1} \mathrm{d}t \: L\bigl(x(t), \dot{x}(t)\bigr) \\
    \frac{\partial S}{\partial x(\bar{t})} &= \int_{t_0}^{t_1} \frac{L\left(x(t) + \epsilon \delta(t-\bar{t}), \dot{x}(t) + \epsilon\frac{\partial \delta(t - \bar{t})}{\partial t}\right) - L\bigl(x(t), \dot{x}(t)\bigr)}{\epsilon}
\end{aligned}$$

## Microcanonical Ensemble
$$\Gamma(E,N, V) \approx \sum_{E' < E} \approx \omega(E,N, V)$$

$$S = k_B \cdot\ln(\Gamma(E, N, V))$$

Ideal Gas
$$S_\text{IG} = Nk_B \cdot\ln\left[\left(\frac{
2\pi m}{h^2} \frac{2E}{3N}\right)^\frac{3}{2} \frac{V}{N}\right]+ \frac{5}{2}N$$

Two microcanonical ensemble in contact.
$$\frac{\partial}{\partial E_A} \ln(\Gamma(E_A)) = \frac{\partial}{\partial E_B} \ln(\Gamma(E_B))$$

$$\frac{1}{k_BT} = \frac{\partial \ln(\Gamma(E))}{\partial E} = \beta$$

$$k_B \frac{\partial \ln(\Gamma(E))}{\partial E} = \frac{\partial Ns}{\partial N\epsilon} = \frac{\partial s}{\partial \epsilon} = \frac{1}{T}$$

$$\frac{\partial T}{\partial E} > 0$$

Chemical Equilibrium
$$\frac{\partial \ln(\Gamma(E_AN_A))}{\partial N_A} = \frac{\partial\ln(\Gamma(E_BN_B))}{\partial N_B}$$

Chemical Potential
$$\mu = -\frac{1}{\beta} \frac{\partial\Gamma}{\partial N} = -\frac{1}{T}\left(\frac{\partial S}{\partial N}\right)_{E,N}$$

$$\frac{\partial \ln(\Gamma(E_AN_AV_A))}{\partial V_A} = \frac{\partial\ln(\Gamma(E_BN_BV_B))}{\partial V_B}$$

Pressure
$$P = \frac{1}{\beta} \frac{\partial\ln(\gamma)}{\partial V} = T \left(\frac{\partial S}{\partial V}\right)_{E,N}$$

$$\mathrm{d}E = T \mathrm{d}S - P\mathrm{d}V + \mu \mathrm{d}N$$

First Law of Thermodynamics
$$\mathrm{d}E = \delta q - \delta\omega$$

Second Law of Thermodynamics
$$T\mathrm{d}S = \delta q$$

Third Law of Thermodynamics
$$T \to 0 \quad E \to E_0 \quad S \to k_B \ln(\Gamma(E_0))$$

### Ideal Gas
Thermal Wavelength
$$\lambda_T = \frac{h}{\sqrt{2\pi m k_BT}}$$

Sackur Tetrode Entropy
$$S(T) = Nk_B \ln\left(\frac{V}{\lambda_T^3N}\right) + \frac{5}{2}Nk_B$$

Equation of State
$$P = \frac{Nk_BT}{V} \rightarrow PV = Nk_BT$$

Chemical Potential
$$\mu = -k_BT \ln\left(\frac{V}{\lambda_T^3N}\right)$$

### Gibbs Paradox
$$\tilde{S}(T) = Nk_B \ln\left(\frac{V}{\lambda_T^3}\right) + \frac{3}{2}Nk_B$$

## Canonical Ensemble

Partition Function
$$\begin{aligned}
Z &= \sum_{E=0}^{E_\text{Tot}} \Gamma(E) e^{-\beta E} \\
Z &= \sum_{x} e^{-\beta E(x)} \\
Z &= \int \omega(E) e^{-\beta E} \mathrm{d}E \\
Z &= \frac{1}{N! h^3} \int e^{-\beta E(x,p)} \mathrm{d}x\:\mathrm{d}p
\end{aligned}$$

Ensemble Average
$$\langle A \rangle = \frac{\sum_{E} A(E) e^{-\beta E}}{Z}$$

$$\langle A(x) \rangle = \frac{\sum_{x} A(x) e^{-\beta E(x)}}{Z}$$

Energy
$$\langle E \rangle = - \frac{\partial}{\partial \beta} \ln(Z)$$

$$\begin{aligned}
\Delta E^2 &= -\frac{\partial}{\partial \beta} \langle E\rangle \\
&= k_BT \frac{\partial\langle E\rangle}{\partial T} \\
&= k_BT C_V
\end{aligned}$$

Heat Capacity
$$C_V = \left(\frac{\partial \langle E\rangle}{\partial T}\right)_V$$

$$P(E) = \frac{\Gamma(E) e^{-\beta E}}{Z}$$

$$P(x) = \frac{\Gamma(E) e^{-\beta E(x)}}{Z}$$

Instantaneous Force (and Pressure)
$$\begin{aligned}
\delta W &= F_x\mathrm{d}x \\
&= \frac{F_x}{A} A \mathrm{d}x \\
&= P \mathrm{d}V \\
&= \frac{-\sum_E e^{-\beta E} \Gamma(E) \frac{\partial E}{\partial x} \mathrm{d}V}{AZ} \\
P &= \frac{-\sum_E e^{-\beta E} \Gamma(E)}{Z}\frac{\partial E}{\partial V} \\
&= \frac{1}{\beta} \frac{1}{Z} \frac{\partial Z}{\partial V} \\
&= k_BT \frac{\partial \ln(Z)}{\partial V}
\end{aligned}$$

$$\begin{aligned}
\mathrm{d}\ln(Z) &= \beta \underbrace{P \mathrm{d}V}_{\delta W} - \langle E\rangle \mathrm{d}\beta \\
&= -\mathrm{d}(\beta\langle E\rangle) + \beta \delta q \\
&= -\mathrm{d}(\beta\langle E\rangle) + \frac{\mathrm{d}S}{k_B} \\
\mathrm{d}(\ln(Z) + \beta\langle E\rangle) &= \frac{\mathrm{d}S}{k_B} \\
S &= k_B \mathrm{d}(\ln(Z) + \beta\langle E\rangle)
\end{aligned}$$

$$-k_BT \ln(Z) = \langle E\rangle - TS$$

Helmholz Free Energy
$$F = \langle E\rangle - TS$$
$$F = -k_BT \ln(Z)$$

$$Z = \int \exp\left(\frac{S}{k_B} - \frac{E}{k_BT}\right) \mathrm{d}E$$
$$Z = \int \exp(-\beta F(E)) \mathrm{d}E$$

$F$ is minimal at the equilibrium.

$$\langle E\rangle = F - T \frac{\partial E}{\partial T}$$

$$S = -\frac{\partial F}{\partial T}$$
$$S = k_B\ln(Z) + \beta \langle E\rangle$$

### Ideal Gas
Hamiltonian
$$H = \sum \frac{p^2}{2m}$$

$$\begin{aligned}
Z &= \frac{1}{N!h^{3N}} \int \mathrm{d}p e^{-\beta \sum \frac{p^2}{2m}} \int \mathrm{d}x \\
&= \frac{V^N}{N! h^{3N}} \prod_{i=1}^{3N} \int_{-\infty}^\infty \mathrm{d}p_i e^{-\frac{\beta p^2}{2m}} \\
&= \frac{\sqrt{2\pi mk_BT}^{3N}}{N! h^{3n}} V^N \\
&= \frac{1}{N!} \frac{V^N}{\lambda_T^{3N}}
\end{aligned}$$

$$F = Nk_BT \ln\left(\frac{N\lambda_T^3}{V}\right) - Nk_BT$$

$$\begin{aligned}
S &= Nk_B - Nk_B \ln\left(\frac{N \lambda_T^3}{V}\right) - \frac{3Nk_BT}{\lambda_T} \frac{\partial\lambda_T}{\partial T} \\
&= Nk_B - Nk_B \ln\left(\frac{N \lambda_T^3}{V}\right) + \frac{3}{2}Nk_B \\
&= Nk_B \Biggl(\frac{5}{2} - \ln\left(\frac{N \lambda_T^3}{V}\right)\Biggr)
\end{aligned}$$

$$P = \frac{Nk_BT}{V}$$
$$PV = Nk_BT$$

Molecular Partition Function
$$z_0 = \int \frac{\mathrm{d}q\mathrm{d}p}{h^3} e^{-\beta \frac{p^2}{2m}}$$
$$Z = \frac{1}{N!} (z_0)^N$$

$$\langle E\rangle = \frac{3}{2} Nk_BT$$

Estimate Velocity
$$\bar v = \sqrt{\langle v^2 \rangle} = \sqrt{\frac{3k_BT}{2}}$$

Chemical Potential
$$\mu = -\frac{1}{\beta} \frac{\partial \ln(Z)}{\partial N} = \frac{\partial F}{\partial N}$$

Pressure
$$P = \frac{\partial F}{\partial V}$$

## Grand Canonical Ensemble $(T, V, \mu)$

$$P(x) = \frac{e^{-beta E(x) + \beta \mu N(x)}}{\Xi}$$

$$\Xi = \sum_{N=0}^\infty \sum_x e^{-\beta E(x) + \beta \mu N}$$
$$\Xi = \sum_{N=0}^\infty Z_N \cdot e^{\beta \mu N}$$

$$Z_N = e^{-\beta F}$$

$$Z_\text{Tot} = \frac{1}{h^{3N_\text{Tot}}} \sum_{N_2=0}^{N_\text{Tot}} \binom{N_\text{Tot}}{N_2} \int \mathrm{d}x_1 \mathrm{d}p_1 e^{-\beta H_1(x_1,p_1)} \int \mathrm{d}x_2 \mathrm{d}p_2 e^{-\beta H_2(x_2, p_2)}$$

$$Z_\text{Tot} = \sum_{N_2=0}^{N_\text{Tot}} Z_{N_1} Z_{N_2}$$

$$p(x_2, p_2, N_2) = \frac{Z_{N_1} e^{-\beta H_2}}{h^{3N_2}N_2! Z_\text{Tot}}$$

$$p(x_2, p_2, N_2) = \frac{e^{-\beta H_2(x_2,p_2) - \beta PV_2 + \beta \mu N_2}}{h^{3N_2}N_2!}$$

$$\Xi = e^{\beta PV_2}$$

Grand Potential (Equation of State)
$$\Omega = -k_BT \ln(\Xi) = -PV$$

$$\langle N \rangle = \sum_{N=0}^\infty \frac{N Z_N e^{\beta \mu N}}{\Xi}$$
$$\langle N\rangle = \frac{1}{\beta} \frac{\partial \ln(\Xi)}{\partial \mu}$$

$$\langle N^2\rangle_c = \left(\frac{1}{\beta}\frac{\partial}{\partial \mu}\right)^2 \ln(\Xi)$$

Equation of State
$$\frac{PV}{k_BT} = \beta \mu N - \beta F$$
$$PV = \mu N - F$$

$$PV = \mu N - E + TS$$

Fundamental Equation of Thermodynamics
$$E = TS- PV + \mu N$$

$$\mathrm{d}F = \mu\mathrm{d}N - P\mathrm{d}V - S\mathrm{d}T$$

$$\mathrm{d}\Omega = -P\mathrm{d}V - S\mathrm{d}T - N\mathrm{d}\mu$$

Pressure
$$P = -\frac{\partial \Omega}{\partial V}$$

Entropy
$$S = -\frac{\partial \Omega}{\partial T}$$

Number of particles
$$N = \frac{\partial \Omega}{\partial \mu}$$

Gibbs Duhem Equation
$$V\mathrm{d}P - S\mathrm{d}T - N\mathrm{d}\mu = 0$$

$$\mu = f(v, T) - v\frac{\partial f}{\partial v}$$

$$P = -\frac{\partial f}{\partial v}$$

$$\frac{\partial\mu}{\partial v} = v \frac{\partial P}{\partial v}$$

Isothermal Compressibility
$$\Kappa_T = -\frac{1}{V} \frac{\partial V}{\partial P} > 0$$

Sackur Tetrode Equation
$$S = Nk_B \ln\left(\frac{V}{N\lambda_T^3}\right) + \frac{5}{2} Nk_B$$

Saha Equation
$$\frac{n_pn_e}{n_H} = e^{-\beta I}\left(\frac{mk_BT 2 \pi}{h^2}\right)^\frac{3}{2}$$
