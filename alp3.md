# Algorithmen, Datenstrukturen und Datenabstraktion

*18.10.2022 Vorlesung `I`*

## Themen
+ Datenstrukturen (Listen, Bäume)
+ Datenzugriff/-abstraktion (Interface, Implementierung)
+ Algorithmen (Datenmanipulation)

## Ziele
+ Methoden zum Umgang mit Daten
+ Analyse von Methoden

---
*20.10.2022 Vorlesung `II`*
## Polynome als Datentyp
### Koeffizientendarstellung
```
p[0], p[1], p[2], ..., p[n]
```
Auswerten:
```python
def auswerten(p, x0):
    a = 0
    x = 1
    for i in range(n):
        a = a + p[i] * x
        x = x * x_0
    return a
```

### Alternative Darstellung
Seien $\alpha_0, \alpha_1, \ldots, \alpha_n$ und $\beta_0, \beta_1, \beta_2, \ldots, \beta_n$ paarweise verschieden.

#### Wertdarstellung
Um ein Polynom vom Grad $n$ darzustellen, wählen wir einmal $n+1$ feste verschiedene Zahlen $\alpha_i$ und speichern $p(\alpha_i)$ in einem Array.

Addition: $\mathcal{O}(n)$
Multiplikation: $\mathcal{O}(n)$
Auswerten: $\mathcal{O}(n^2)$

Für jedes $n \in \mathbb{N}$ existiert eine magische Zahl $\omega$ mit
$$\omega \ne 0,\quad \omega^{n+1} = 1$$
$\omega^i$ sind paarweise verschieden
$$\omega = e^{i\frac{2\pi}{n+1}}$$

Setze $\alpha_i = \omega^i,~\alpha_1 = \omega, \ldots,~\alpha_n = \omega^n$

Separiere gerade und ungerade Exponenten $\rightarrow$ Fast-Fourier-Transform (FFT)

---
*25.10.2022 Vorlesung `III`*

### Polynome
+ Koeffizientendarstellung
+ Wertedarstellung

schneller Wechsel der Darstellung?\
Idee: geschickte Wahl der Stellen für die Wertedarstellung

magische Zahl $\omega = e^{i\frac{2\pi}{n+1}}$ \
Wähle $\omega^j$ für die Stellen der Wertedarstellung

Um ein Polynom $p$ von der Koeffizientendarstellung in die Werte darstellung zu überführen müssen wir $p(\omega^j)$ berechnen.

Trick:
$$p(x) = \sum_{i=0}^n a_i \cdot x^i = ax^3 + bx^2 + cx + d$$
$$\begin{aligned}
    p(x) &= ax^3 + cx + bx^2 + d \\
    &= x (ax^2 + c) + bx^2 + d \\
    &\text{Setze } q(x) = ax + c \quad r(x) = bx + d \\
    &= x \cdot q(x^2) + r(x^2) \\
    p(\omega^i) &= \omega^i \cdot q(\omega^{2i}) + r(\omega^{2i}) \\
    &\enspace \vdots
\end{aligned}$$

Anzahl der Operationen:
$$\begin{aligned}
T(n) &= 2 \cdot T\left(\frac{n}{2}\right) + 2n \\
&\in \mathcal{O}(n \cdot \log(n))
\end{aligned}$$

$\rightarrow$ Man kann mit $\propto n \cdot \log(n)$ Operationen von einer in die andere Darstellung wechseln. (Schnelle Fourier-Transformation)

Bei Daten kommt es stark auf die Darstellung im Rechner an. Je nach gewählter Darstellung, sind manche Operationen schnell und andere langsam.

Die theoretische Analyse gibt uns Möglichkeiten die verschiedenen Lösungen zu bewerten.

---
*27.10.2022 Vorlesung `IV`*

## Algorithmen

Ein Algorithmus ist ein endlich beschriebenes, allgemeines und effektives Verfahren, das eine Eingabe in eine Ausgabe überführt.

Ein guter Algorithmus ist:
+ deterministisch
+ effizient (optimal)
+ korrekt
+ robust
+ einfach

Wie bewertet man einen Algorithmus:
1. Ausprobieren
2. Analysieren

### 1. Ausprobieren/Benchmarking

Vorteile:
+ sehr konkret
+ handfeste Daten

Nachteile:
+ beeinflusst durch Hardware, Programmiersprache, usw.
+ wenig Verkleichbarrkeit
+ nur endlich viele Eingaben können getestet werden

### 2. Theoretische Analyse

Darstellung der Laufzeit als Funktion in Abhängigkeit der Eingabegröße

Wir konzentrrieren uns auf den schlimmsten Fall (Worst-Case)

Wir zählen die Schritte und Speicherzellen. \
Dazu benötigen wir ein abstraktes Maschinenmodell (z.B. Turing-Maschine)

Modell der Wahl = Registermaschine (RAM)

Die RAM modelliert einen einfachen Rechner. Jede Zelle speichert eine ganze Zahl

CPU: fester Befehlssatz
+ arithmetische Operationen
+ Sprünge, bedingte Sprünge
+ `MOVE` (Speicherzelle kopieren)
+ indirekte Addressierung

Ein Algortihmus ist ein Programm für die RAM

Nun können wirr genau sagen, was die theoretische Laufzeit eines Algorithmus ist.

$T_A(I):$ # der Befehle, welche die RAM bei Eingabe $I$ ausführt

Eingabegröße hängt vom Problem ab:
+ Sortieren: Anzahl der Elemente
+ Polynomoperation: Grad des Polynoms

Wir schreiben den Algorithmus im Pseudocode. Aufwand ist für jede Zeile des Pseudocode gleich der Anzahl an Anweisung einer vernünftigen Registermaschine.

---
*01.11.2022 Vorlesung `V`*
## $\mathcal{O}$-Notation

Sei $f, g: \mathbb{N} \rightarrow \mathbb{R}$. Dann gilt:
$$\begin{aligned}
    f \in \mathcal{O}(g) &\Longleftrightarrow \exists c > 0, n_0 \ge 1, \forall n \ge n_0: f(n) \le c \cdot g(n) \\
    &\Longleftrightarrow \limsup_{x \rightarrow \infty} \left|\frac{f(x)}{g(x)}\right| < \infty \\
    f \in \Omega(g) &\Longleftrightarrow \exists c > 0, n_0 \ge 1, \forall n \ge n_0: f(n) \ge c \cdot g(n) \\
    &\Longleftrightarrow \liminf_{x \rightarrow \infty} \left|\frac{f(x)}{g(x)}\right| > 0 \\
    f \in \Theta(g) &\Longleftrightarrow f \in \mathcal{O}(g) \land f \in \Omega(g)
\end{aligned}$$

---
*03.11.2022 Vorlesung `VI`*
## Abstrakte Datenstrukturen

### LIFO (Last In First Out): Stapel/Stack
+ `push(x)`: legt $x$ auf den Stack
+ `pop()`: entfernt oberstes Element vom Stack und gibt es zurück
+ `top()`: gibt das oberste Element zurück
+ `size()`: gibt die Anzahl der Elemente zurück
+ `isEmpty()`: gibt zurück, ob der Stack leer ist

### FIFO (First In First Out): Schlange/Queue
+ `enqueue(x)`: hängt $x$ an die Schlange an
+ `dequeue()`: entfernt das erste Element und gibt es zurück
+ `first()`: gibt das erste Element zurück
+ `size()`: gibt die Anzahl der Elemente zurück
+ `isEmpty()`: gibt zurück, ob die Schlange leer ist

---
*08.11.2022 Vorlesung `VII`*

Stack-/Queue-Implementierung mit verketteten Listen:
```yaml
Stack: 2 -> 7 -> 4 -> 6
Queue: 6 -> 4 -> 7 -> 2
```
nach den Aufrufen:
+ `push(6)` bzw. `enqueue(6)`
+ `push(4)` bzw. `enqueue(4)`
+ `push(7)` bzw. `enqueue(7)`
+ `push(2)` bzw. `enqueue(2)`

Vorteil von verketteten Listen: variable Größe \
Nachteil von verketteten Listen: langsamer als Array \
Lösung: Dynamische Arrays

```python
def push(x):
    resize()
    A[head++] = x
```

`resize` überprüft, ob zugrundeliegendes Array voll ist. Wenn ja, wird neues Array der doppelten Größe angelegt und alle Elemente vom alten Array werden in das neue Array rüberkopiert.

***Def.:*** **Amortisierte Laufzeitanalyse** \
Gegeben seien verscheidene Operationen auf einer Datenstruktur und $(A_i)_{1\le i\le m}$ eine Folge dieser Operationen. Dann heißt
$$\frac{T\Bigl((A_i)_{1\le i\le m})\Bigr)}{m} = \frac{\sum_{i=1}^m T(A_i)}{m}$$
amortisierte Laufzeit.

***Satz:*** Jede Folge von `push`/`pop` Operationen benötigt insgesamt $\mathcal{O}(n)$ Zeit (jede Operation läuft in $\mathcal{O}(1)$ amortisiert ab).


## Prioritätswarteschlange
Anwendunggebiete:
+ Scheduling
+ Dijkstra-Algorithmus

***Def.:*** \
Gegeben sei ein totalgeordnetes Universum $(U, \le)$. Die Elemente der DS sind eine Teilmenge $S \subseteq U$. Sie bietet folgende Operationen an:
+ `findMin`
    + Vor.: $S \ne \varnothing$
    + Effekt: keine
    + Ergebnis: $\min(S)$
+ `deleteMin`
    + Vor.: $S \ne \varnothing$
    + Effekt: $S \leftarrow S\setminus\{\min(S)\}$
    + Ergebnis: $\min(S)$
+ `insert(x)` mit $x \in U$
    + Vor.: $|S| < \texttt{MAXSIZE}$
    + Effekt: $S \leftarrow S \cup \{x\}$
    + Ergebnis: keins
+ `size`
    + Vor.: keine
    + Effekt: keine
    + Ergebnis: $|S|$
+ `isEmpty`
    + Vor.: keine
    + Effekt: keine
    + Ergebnis: $S \overset{?}{=} \varnothing$

Über die Spezifikation von abstrakten Datenstrukturen:
+ Beschreibt die Funktionalität der einzelnen Operationen, sowie Elemente der DS
+ erfolgt mathematisch, prosa, in einer Programmiersprache
+ für jede Operation müssen 3 Eigenschaft spzifiziert werden:
    + Vorraussetzung: Gibt an, unter welchen Bedingungen die Operation aufgerufen werden darf
    + Effekt: Gibt die Änderung an, die in der DS vorgenommen werden sollen
    + Ergebnis: Gibt den zurückgegebenen Wert der Operation an

Mögliche Implementierungen:
1. unsortierte Liste/Array
    + `findMin` $\in \mathcal{O}(n)$
    + `deleteMin` $\in \mathcal{O}(n)$
    + `insert` $\in \mathcal{O}(1)$
2. sortierte Liste/Array
    + `findMin` $\in \mathcal{O}(1)$
    + `deleteMin` $\in \mathcal{O}(1)$
    + `insert` $\in \mathcal{O}(n)$
3. binärer Heap
    + `findMin` $\in \mathcal{O}(1)$
    + `deleteMin` $\in \mathcal{O}(\log(n))$
    + `insert` $\in \mathcal{O}(\log(n))$

---
*10.11.2022 Vorlesung `VIII`*

### Binärer Heap

Ein binäre Heap besitzt zwei Eigenschaft:
+ Vollständigkeit: Alle Ebenen müssen komplett gefüllt sein. Die unterste Ebene muss von links aufgefüllt sein.
+ Ordnungseigenschaft: Bei Min-Heap sind die Kinderknoten größer oder gleich dem Elternknoten.

```python
def findMin():
    """Gibt die Wurzel zurück"""
```

```python
def deleteMin():
    """Merke die Wurzel und setze das letzte Element in die Wurzel. Versickere die Wurzel"""
```

```python
def versickern(i):
    if l(i) >= size or heap[l(i) >= heap[i] and r(i) >= size or heap[r(i)] >= heap[i]:
        return
    else:
        if heap[l(i)] < heap[r(i)] or r(i) >= size:
            heap[i], heap[l(i)] = heap[l(i)], heap[i]
            versickern(l(i))
        else:
            heap[i], heap[r(i)] = heap[r(i)], heap[i]
            versickern(r(i))
```

```python
def insert(x):
    """Hänge x an das Ende an und blubber es hoch."""
```

```python
def hochblubbern(i):
    while i > 0:
        if heap[i] < heap[p(i)]:
            heap[i], heap[p(i)] = heap[p(i)], heap[i]
            i = p(i)
        else:
            break
```

---
*15.11.22 Vorlesung `IX`*

## Binäre heaps

Sei `A` ein Array mit $n$ Elementen (aus einem geordenten Universum). Wie lange dauert es um aus `A` einen binären Heap zu erstellen.

$$n \cdot insert = n \cdot \log(n)$$

Aber es geht schneller.

Mit `hochblubbern` wär das dann $\mathcal{O}(n)$?

Für $n$ inserts gilt
$$\#\text{Swaps} = \sum_{i=0}^{log(n)-1} (2^i)*i$$

## Hash-Tabellen
Kann Einträge speichen, löschen und nachschlagen (Unabhänig von der Position, Eigenschaften des Eintrag)

Seien $K$ (`key`-Menge) und $V$ (`value`-Menge) zwei Mengen, dann gilt für $S$ (Hash-Tabelle)
$$S \subset KxV$$

Eine Hashtabelle unterstützt folgende Operationen:
+ `put(k,v)`
    + Vor.: keine
    + Effekt S von (S\{(k,v`),(k,v`)€S}) und (k,v)
	+ Ergebnis: keins
+ `get(k)`
    + Vor.: $\exists k: (k, v) \in S$
    + Effekt: keiner
    + Ergebnis: $v : (k, v) \in S$
+ `remove(k)`
    + Vor.: $\exists k: (k, v) \in S$
    + Effekt: $(k, v)$ wird aus $S$ entfernt
    + Ergebnis: keins

### Implementierung
K-Liste z.B $k=\{1,..,100\}$:
+ Array der Größe 101
+ `put(k,v): A[k] = v`
+ `get(k,v): return A[k]`
+ `remove(k,v): del A[k]`
+ Alle in O(1)

Warum gilt das nicht allgmein

1. k kann beliebig sein, nicht notwendiger weise aufeinanderfolgend oder ganze Zahlen

2. k kann sehr Groß sein, S nicht Unbedingt

Lösung: Eine „gute“ Funktion `h(k)` nach 0 bis n-1 abbildet, sodass man statt `k` `h(k)`als Index benutzt. h heißt dasnn Hashfunktion

Probleme:
+ key größer als Array
+ mehrere keys zeigen auf den selben Index (Kollision)

3 Möglichkeiten.

1. `A[i]` speichert alle Einträge `(k,v)` mit `H(k) = i` (Verkettung)
2. Wenn `A[h(k)]` schon besetzt ist, finde einen Anderen platz (offene Adressierung)
3. Wenn `A[h(k)]` besetzt, dann schaffe Platz. (Kuckuck-Hashing)

Array der Länge $N$ \
Hashfunktion h(k) von 0 nach n-1 \
alles zusamm Hashtabbele \
Strategie zur kollsionsbehebung

Wahl der hashfunktion \
Ziel das jeder key von K ein wert von 0 bis N-1 bekommt \
Dies soll m öglichst „zufällig“ sein. \
H soll möglichst zur vorkommenden Struktur der zu speichernden Menge der Schlüssel sortieren \
H wird in 2 schritten berechnet: a)berechne hash für k d.h. eine große zahl für k \

Bsp.
+ K€N: nichts zu tun
+ K€Q: `floor(k)`
+ K€stringk=a1,…aj: dann k Alphabet von ASCII

b) kommpression bilde k auf 0 bis n-1 ab

---
*17.11.2022 Vorlesung `X`*

Normalerweise gilt $|S| \ll |K|$

Wir verwenden ein Arrayder Größe $N$ und eine Funktion $h: K\rightarrow \{0, 1, \ldots, N-1\}$, welche gut streut um Kollisionen zu vermeiden.

$$h : K \rightarrow \mathbb{Z} \rightarrow \{0, 1, \ldots, N-1\}$$

$K \rightarrow \mathbb{Z}$: Hashwert\
$\mathbb{Z} \rightarrow \{0,1, \ldots, N-1\}$: Kompression

Die Kompression soll gut streuen, bzw. gleichmäßig verteilen.

Beispiel: Sei $z \in \mathbb{Z}$
$$z \mapsto (z \mod p) \mod N$$
wobei $p$ eine Primzahl mit $p > N$ ist.

Besseres Beispiel:
$$z \mapsto ((a \cdot z + b) \mod p) \mod N$$
mit $a, b \in \{0, 1, \ldots, p-1\}$ zufällig gewählt.

### Verkettung
`A[i]` speichert alle  $(k,v) \in S$ mit $H(k) = i$ in eine verkettete Liste.

Speicherbedarf $\in \mathcal{O}(N + |S|)$\
Laufzeit:
+ Best-Case: $\mathcal{O}(1)$
+ Worst-Case: $\mathcal{O}(S)$

Annahme für die Average-Case Analyse:\
Sei $h$ eine zufällige Funktion (gleich verteilt) dann gilt
$$p\Bigl[h(k) = i\Bigr] = \frac{1}{N}$$
und alle Werte sind für alle $h(k)$ unabhängig.

Fixiere $S$ und $k$. Sei
$$\begin{aligned}
L_k &= \left\{\begin{matrix}
    1 :&\text{falls $k(k') = h(k)$} \\
    0 :&\text{sonst}
\end{matrix}\right. \\
L &= 1 + \sum_{(k', v) \in S} L_{k'} \\
E[L] &= 1 + \sum_{(k',v) \in S} E[L_{k'}] \\
&= 1 + \sum_{(k',v)\in S} \frac{1}{N} \\
&= 1 + \frac{|S| - 1}{N} \\
\lim_{N \rightarrow \infty} E[L] &= 1 + \frac{|S|}{N}
\end{aligned}$$
mit $\frac{|S|}{N} \hat{=}$ Ladefaktor. Wenn $|S| \in \mathcal{O}(n)$, dann ist die erwartete Laufzeit von `put`/`get`/`remove` konstant.

**Achtung**: In der nalyse waren $S$ und $k$ fixiert uund unabhängig vonder Hashfunktion $h$. Wäre das nicht der Fall, stimmt diese Analyse nicht (siehe Übung).

Problem: Annahme, dass $h$ zufällig gewählt wird ist nich praktikabel, aber man kann $h$ pseudo-zufällig aus einer kleineren Familie von Funktionen wählen (z.B. universelles Hashing) und die Analyse klappt dann immer noch.

Bemerkung: Der Ladefaktor soll in $\mathcal{O}(1)$ bleiben. Wenn die Hashtabelle zu voll wird, verdopple die Größe $N$ des Arrays und baue die Hashtabelle neu auf.

| Vorteile | Nachteile |
| -------- | --------- |
| praktikabel | verschwendet Speicher |
| einfach |

### Offene Adressierung (mit linearem Sondieren)
Wenn `A[h(k)]` besetzt ist, finde eine neue Position rechts daneben (`A[h(k)+1]`).

```python
def put(k, v):
    i = h(k)
    while A[h(k)] is not None:
        i = (i+1)%len(A)
        if h(k) == i:
            # vergrößere A
    A[i] = k, v
```

```python
def get(k):
    i = h(k)
    while A[i] is not None and A[i][0] != k:
        i = (i+1)%len(A)
        if i == h(k):
            raise ValueError
    if A[i] is None:
        raise ValueError
    return A[i]
```

```python
def remove(k):
    i = h(i)
    while A[i] is not None and A[i][0] != k:
        i = (i+1)%len(A)
        if h(k) == 1:
            raise ValueError
    A[i] = None
```

**Vorsicht:**
1. Brauchen Sonderzeichen beim Löschen
2. Klumpenbildung: ggf. andere Sondierungsreihenfolge
3. Es muss gelten $|S| \le N$. Rehashing, wenn $S$ zu groß wird.

Wenn der Ladefaktor als 1 und $h$ zufällig gewählt ist, benötigen alle Operationen $\mathcal{O}(1)$ erwartete Laufzeit.

---
*22.11.2022 Vorlesung `XI`*

+ durch das Löschen wird die Tabelle vermüllt $\rightarrow$ baue die Tabelle in regelmäßigen Abständen neu auf
+ lineares Sondieren bietet gute Cache-Performance,führt aber zu Klumpenbildung (kann durch alternative Sondierung verbessert werden, z.B. quadratisches Sondieren)

### Kuckucks Hashing
+ Wenn `A[h(k)]` besetzt ist, schaffe Platz
+ Man verwendet 2 verschiedene Hash-Funktion $h_1$ und $h_2$, die zufällig aus einer Menge von Hash-Funktionen gewählt werden
+ Ein Objekt mit Schlüssel $k$ steht entweder in `A[`$h_1(k)$`]` oder in `A[`$h_2(k)$`]`
+ d.h. `get`/`remove` müssen nur 2 Positionen prüfen
+ bei `put` werden Objekte ggf. auf eine alternative Position verschoben

```python
def get(k):
    if A[h1(k)] is not None and k == A[h1(k)].k:
        return A[h1(k)].v
    elif A[h2(k)] is not None and k == A[h2(k)].k:
        return A[h2(k)]
    else:
        raise IndexError
```

```python
def remove(k):
    if A[h1(k)] is not None and k == A[h1(k)].k:
        A[h1(k)] = None
    elif A[h1(k)] is not None and k == A[h2(k)].k:
        A[h2(k)] = None
    else:
        raise IndexError
```

```python
def put(k, v): # iterativ
    if A[h1(k)] is None or A[h1(k)].k == k:
        A[h1(k)] = (k, v)
    elif A[h1(k)] is None or A[h2(k)].k == k:
        A[h2(k)] = (k, v)
    else:
        k1 = A[h1(k)].k
        k2 = A[h2(k)].k
        if A[h2(k1)] is None:
            A[h2(k1)] = A[h1(k1)]
            A[h1(k)] = (k, v)
        elif A[h1(k2)] is None:
            A[h1(k2)] = A[h2(k2)]
            A[h2(k)] = (k, v)
        else:
            # vergrößer das Array
```

`put` kann ggf. lange dauern und/oder komplett scheitern. Wenn der Ladefaktor klein genug istund $h_1$ und $h_2$ zufällig und unabhängig gewählt wurden, dann benötigt `put` $\mathcal{O}(1)$ durchschnittliche amortisierte Laufzeit.

| Vorteil | Nachteile |
| ------- | --------- |
| platzsparend | `put` kann lange dauern und scheitern |
| Schnelle Laufzeit für `get`/`remove` | viel Datenbewegung bei `put` möglich |
| gut in der Praxis |

Weitere Anwendungen von Hash-Tabellen:
+ Hashfunktionen sind einfach ($\mathcal{O}(1)$) zu berechnen
+ kann potentiel unendliches, bzw. großes Schlüsseluniversum auf einen vergleichsweise kleinen Zahlenbereich abbilden
+ soll/kann gut streuen, was Kollisionen vermeidet

Angenommen wir haben eine gut streuende Hashfunktion, dann kann man sie als Fingerabdruck/Prüfsumme für Dateien verwenden (z.B. Datenübertragung). Wenn $N$ groß genug ist, dann ist eine Kollision sehr unwahrscheinlich. Wenn $h$ zufällig ist, dann müsste ein Gegner im Erwartungswert $N$ Urbilder ausprobieren, um eine Datei zu finden, welche die gleiche Prüfsumme besitzt.

---
*24.11.2022 Vorlesung `XII`*
### Kryptografische Hashfunktionen

**Definition:** \
Eine Funktion $h: \sum^* \rightarrow \{0, \ldots, N-1\}$ heißt kryptografische Hashfunktion, wenn es schwierig ist, zu gegebenen, genügend zufälligen $\omega \in \sum^*$ ein $\omega' \neq \omega$ zu finden, mit $h(\omega) = h(\omega')$ und wenn sich $h$ schnell berechnen lässt.

**Vermutung/Hoffnung:** \
kryptografische Hashfunktionen existieren

Es sind keine krypt. Hashfunktionen bekannt.Es gibt aber heuristische Funktionen, von denen man annimmt, dass sie in der Praxis funktionieren,wie oben beschrieben.

Mithilfe von krypt. Hashfunktionen kann man Daten mit einem praktisch eindeutigen Fingerabdruck versehen.

Anwendung in einer Datenstruktur: Hashzeiger
+ Zusammen mit einem Verweis und einem Objekt wird der kryptografische Hash des Objektes gespeichert
$$ h(\text{head}) \rightarrow (o_3, h(o_2)) \rightarrow (o_2,h(o_1)) \rightarrow (o_1, h(o_0)) \rightarrow \perp$$

Durch $h(\text{head})$ idt die komplette Liste praktisch eindeutig festgelegt. Die Liste kann einfach am Kopf wachsen, aber einzelne Knoten lassen nicht ändern, ohne dass es bemerkt wird. \
$\Rightarrow$ Blockchain/Cryptocurrency

## Geordnetes Wörterbuch/Dictionary
Sei die Schlüsselmenge $K$ totalgeordnet und die Werte $V$. Die zu speichernde Menge ist dann, $S \subseteq K \times V$

`put`/`get`/`remove` sind wie bisher implementiert. Es existieren aber weiter Methoden:
+ `min()`: Bestimme kleinsten Schlüssel in $S$
+ `max()`: Bestimme größten Schlüssel in $S$
+ `succ(k)`: Bestimme kleinsten Schlüssel $k'$ in $S$ mit $k' > k$
+ `pred(k)`: Bestimme größten Schlüssel $k'$ in $S$ mit $k' < k$

Die Implementierung mittels Hashtabelle ist ungeeignet, da die Ordnung i.A. verloren geht.

Verkettete Listen (sortiert/unsortiert) sind zu langsam ($\mathcal{O}(n))$

Idee: Speicher $S$ in einer Hierarchie von verketteten Listen.

$L_1$ speichert jedes zweite Objekt von $L_0$\
$L_i$ speichert jedes zweite Objekt von $L_{i-1}$

Die entsprechenden Einträge sind verlinkt. Suche nach einem $k$ in der Schlüsselmenge (`succ`, `pred`, `get`):
+ Beginne in der höchsten Liste
+ Bestimme in $L_i$ das Intervall $\Delta_i$, das $k$ enthält (Binäre Suche)
+ Folge vertikalen Zeigern nach $L_{i-1}$
+ wiederhole bis $k$ in $L_0$ gefunden ist.

$\Longrightarrow$ Laufzeit ist in $\mathcal{O}(\log(n))$

**Problem:** \
Wie erhalten wir die Struktur beim Einfügen Löschen?

**Lösung:** \
Benutze Zufall: Ordne jedem Objekt eine zufällige Höhe zu und hoffe dass die Laufzeit sich fast (im Erwartungswert) wie die vorherigen Listen $L_i$ (mit perfekter Balancierung) verhält.

### **Skipliste:**
`put(k, v)`
+ suche nach $k$
    + wenn bereits enthalten, dann überschreibe den Wert mit $v$
    + ansonsten, wirf eine Münze bis Zahl kommt (geometrische Verteilung) und füge $k$ in die Liste $L_{\text{Anzahl der Würfe} - 1}$ und darunter ein. Lege ggf. neue Listen an.

`remove(k)`
+ suche k
    + lösche $k$ aus allen $L_i$
    + alle anderen Operationen sind wie davor

```python
def put(k, v):
    if k in A:
        A[k] = v
    else:
        m = 0
        while random.random() < 0.5:
            m += 1
        for i in reversed(range(k)):
            L[m].insert(k)
        A[k] = v
```

---
*29.11.2022 Vorlesung `XIII`*

- *Beweis für die Laufzeit von `get` $\in \mathcal{O}(\log(n))$*

| Vorteile | Nachteile |
| -------- | --------- |
| Einfach | schlechte Worst-Case-Laufzeit |
| gute Average-Case-Laufzeit | zusätzlicher Speicher wird benötigt |

### **Binäre Suchbäume**
Ein Baum ist ein kreisfreier, zusammenhängender Graph. In einem gewurzelter Baumist ein Knoten als Wurzel ausgezeichnet. Die Höhe des Baum ist definiert als die maximale Anzahl der Knoten der Wege aller Knoten zur Wurzel. Wenn $(u, v)$ eine Kante vom Baum und $u$ näher an der Wurzel ist als $v$, dann ist $u$ der Elternknoten von $v$ und $v$ ist der Kinderknoten von $u$. Ein Blatt ist ein Knoten ohne Kinderknoten. In einem binären Baum besitzt jeder Knoten maximal 2 Kinderknoten. In einem binären Suchbaum ist das linke Kind kleiner/gleich und das rechte Kind größer/gleich dem Elternknoten.

Bei der Implementierung des geordneten Wörterbuchs als binären Suchbaum, werden die Einträge in den Knoten des Baumes gespeichert.

---
*01.12.2022 Vorlesung `XIV`*

```python
class Knoten():
    def __init__(k, v, left, right, parent):
        self.key = k
        self.value = v
        self.left = left
        self.right = right
        self.parent = parent
```

`get(x)`
+ beginne bei der Wurzel
+ vergleiche $k$ mit Schlüssel am aktuellen Knoten
+ gehe links/rechts, je nach Vergleichsergebnis

```python
def get(k):
    curr = root
    while curr is not None and curr.key != k:
        if k < curr.key:
            curr = curr.left
        else:
            curr = curr.right
    return curr
```

`put(k)`
+ finde $k$ (wie bei `get`)
+ wenn $k$ vorhanden, überschreibe den Wertvon $k$
+ wenn nicht, füge ein Blatt mit $(k,v)$ hinzu

```python
def put(k, v):
    curr = root
    while curr.key != k:
        if k < curr.key:
            if curr.left is None:
                curr.left = Knoten(k, v, parent=curr)
            else:
                curr.left.value = v
        else:
            if curr.right is None:
                curr.right = Knoten(k, v, parent=curr)
            else:
                curr.right.value = v
```

`remove(k)`
+ finde $k$ (wie in `get`)
+ falls $k$ nicht gefunden: fertig
+ sonst, sei $s$ der knoten der $k$ enthält
+ wenn $s$ ein Blatt ist entferne $s$
+ sonst, suche den Vorgänger $k'$ und ersetze $s$ dadurch
+ suche den Vorgänger $k''$ von $k'$ usw.

```python
def pred(k):
    curr = self.get(k)
    if curr.left is not None:
        maxi = curr.left
        while maxi.right is not None:
            maxi = maxi.right
        return maxi
    else:
        while curr.parent is not None and curr.parent.key > k:
            curr = curr.parent
        return curr.parent
```

Alle Operationen haben eine Laufzeit von $\mathcal{O}(\text{Höhe des Baumes})$ \
Problem: Ein einfacher binäre Baum kann eine Höhe von $n$ haben. \
Idee: Ein perfekter Baum hat eine Höhe von $\mathcal{O}(\log(n))$ \
Modifiziere `put`/`remove`, sodass der Baum jederzeit perfekt ist. Dies kann zu größeren Umbauaktionen führen. Wir können jedoch nicht effizient sicherstellen, dass unser Baum jederzeit perfekt ist. \
$\Longrightarrow$ Anforderungen lockern, sodass die Höhe in $\mathcal{O}(\log(n))$ bleibt, aber `put`/`remove` effizienter werden können.

Dazu gibt es viele Möglichkeiten: z.B. AVL-Bäume (Adelson-Velski und Landis)

#### AVL-Bäume
AVL-Bäume sind höhenbalanciert, d.h. für jeden inneren Knoten gilt: \
Die Höhe der Unterbäume unterscheidet sich um höchstens 1.

Dabei sei die Höhe des leeren Baums -1 und die Höhe eines einzelnen Knoten 0.

---
*06.12.2022 Vorlesung `XV`*

Für den Ausgleich der Höhenuunterschiede brauchen wir sog. Rotationen. Sie sind enfach, lokal ändern nur konstant viele Zeiger und erhalten die BSB-Eigenschaften.

+ Linksrotation
+ Rechtsrotation
+ Rechtslinksrotation
+ Linksrechtsrotation

Wenn ein Knoten $v$ unausgeglichen ist und alle Nachbarn ausgeglichen, dann existiert eine Rotation bei $v$ die $v$ ausgleicht und nach deren Ausführung auch alle Nachbarn ausgeglichen sind. Wir können lokal entscheiden, welche Rotation notwendig ist.

Aber: `put`/`remove` in AVL-Baum:
+ füge ein/lösche wie im BSB
+ gehe Pfad zur Wurzel und führe notwendige Rotationen durch

$\Longrightarrow$ Laufzeit in $\mathcal{O}(\log(n))$, da jede Rotation konstant ist.

Wenn $u$ rechts unausgeglichen ist und beim rechten Kind $v$ gilt: $h_l \le h_r$, so ist $u$ nach einer Linksrotation ausgeglichen.

Fakt:
+ Beim Einfügen muss man höchstens einmal rotieren
+ Beim Einfügen wird ein unausgeglichener Knoten ausgeglichen
+ Beim Löschen gegebenenfalls mehrmals
+ Beim Löschen kann höchsten ein ausgeglichener Knoten unausgeglichen werden
+ Durch Rotationen kann sich die Höhe eines Teilbaums verringern, aber ein Knoten wird nie überausgeglichen.

| Vorteile | Nachteile |
| -------- | --------- |
| $\mathcal{O}(\log(n))$ worst-case für alle Operationen | komplizierte Implementierung |
| kompaktere Darstellung im Verleich zu Skiplisten | Beim Löschen viele Rotationen |

#### Red-Black-Trees
Andere binäre Suchbaumarten (Red-Black-Trees) lockern die Struktur noch weiter, um die Anzahl der Rotationen pro Operationen zu reduzieren. Knoten sind entweder rot oder schwarz gefärbt, nach den folgenden Regeln:
1. Die Wurzel ist schwarz
2. leere Knoten sind schwarz
3. alle Kinder eines roten Knotens sind schwarz
4. alle Pfade von der Wurzel zu einem leeren Knoten enthalten gleich viele schwarze Knoten

Red-Black-Trees haben Höhe $\mathcal{O}(\log(n))$. Jeder AVL-Baum ist ein Red-Black-Tree. Beim Einfügen/Löschen lässt sich die Struktur durch umfärben, bzw. rotieren wieder herstellen. Es genügen jeweils zwei Rotationen im Gegensatz zu AVL-Bäumen beim Löschen. Red-Black-Trees sind sehr verbreitet (Java, Linux-Kernel, etc.)

---
*08.12.2022 Vorlesung `XVI`*

### $(a, b)$-Bäume
Erinnerung:
Wir mussten die Struktur des perfekten Baums lockernum effezientzu bleiben.

Bei AVL-/Rot-Schwarz-Bäumen war die Anzahl der Kinderknoten (Knotengrad) konstant. Bei $(a, b)$-Bäumen kann der Knotengrad variieren. Es gilt:
$$a, b \in \mathbb{N}, a \ge 2, b \ge 2a - 1$$

+ Der Grad aller Knoten ist $\le b$.
+ Der Grad der Wurzel ist $\ge 2$.
+ Der Grad der inneren Knoten ist $\ge a$.
+ Die Tiefe aller Blätter ist gleich.
+ Jeder Knoten speichert $n-1$ Einträge, mit $n$ gleich dem Grad des Knotens.
+ Die Schlüssel in den Knoten sind sortiert.
+ Schlüsselin Unterbäumen folgen der Suchbaumeigenschaft.

Für die Höhe $h$ eines $(a,b)$-Baums mit $n$ Elementen gilt:
$$ 1 + \log_a(n/2) \le h \le \log_b(n)$$
$$ \log_a(n) \le h \le \log_b(n)$$

`get` ist wie bei binären Suchbäumen, wir müssen aber ggf. mehrere Schlüssel pro Knoten vergleichen $\Rightarrow \mathcal{O}(b \cdot \log_a(n))$. Mit binäre Suche innerhalb eines Knotens kann man die Suche verschnellern $\Rightarrow \mathcal{O}(log(b) \cdot \log_a(n))$

`put`: Wir fügen den Schlüssel in das passende Blatt ein. Wenn das Blatt jetzt voll ist (Überlauf), dann spalte das Blatt und füge die Ergebnisse in den Elternknoten ein. Wiederhole das mit dem Elternknoten, bis wir keine Überläufe mehr haben oder an der Wurzel angekommen sind.

`remove`: Wenn der Schlüssel $k$ in einem inneren Knoten sitzt, dann ersetze $k$ mit dem Vörgänger/Nachfolger von $k$. Wenn das Blatt leer ist. Borge ein Schlüssel von einem Geschwisterknoten. Wenn der Geschwisterknoten nicht genügend Schlüssel enthält verschmelze beide Knoten und wiederhole es mit dem Elternknoten.

Siehe auch http://page.mi.fu-berlin.de/mulzer/notes/alp3/ab.pdf

---
*15.12.2022 Vorlesung `XVII`*

## Zeichenketten
Sei $\Sigma$ ein Alphabet. Dann ist eine Zeichenkette eine endliche Folge von Symbolen aus $\Sigma$. Zum Beispiel:
+ $\Sigma = \{a, b, \ldots, z\}$
+ $\Sigma = \{C, G, T, A\}$
+ $\Sigma = \{\alpha, \beta, \gamma, \ldots, \omega\}$

Probleme:
+ Wie ähnlich sind sich zwei Zeichenketten?
+ Enthält die Zeichenkette $\sigma$ die Zeichenkette $\tau$?
+ effizientes Speichern
+ effizientes Wörterbuch

### Effizientes Speichern
Einfach: Weise jedem Zeichen in $\Sigma$ $\lceil \log_2(|\Sigma|) \rceil$ viele Bits zu und speichere Konkatenationen der Kodierungen

Aber: Kann ineffizient sein, wenn einige Buchstaben häufiger vorkommen.

Code: Eine Funktion $C: \Sigma \rightarrow \{0, 1\}^*$ \
Die Codierung eines Wortes $w = w_1 w_2 \ldots w_l$ ist $C(w) = C(w_1) C(w_2) \ldots C(w_l)$

Wir wollen einen Code, der eindeutig zu decodieren ist und der das zu speichernde Wort in einen möglichst kurzen Bitstring codiert.

Eindeutige Dekodierbarkeit: Wenn kein Codewort Präfixe eines anderen Codeworts ist, heißt der Code präfixfrei. Jeder präfixfreie Code ist eindeutig, aber nicht jeder eindeutig dekodierbare Code ist präfixfrei.

Beobachtung: Ein präfixfreier (binärer) Code entspricht einem binärem Baum.

Problem:
+ geg.: Sei $\Sigma$ ein Alphabet mit den Häufigkeiten $h: \Sigma \rightarrow \mathbb{N}$, welche die Anzahl der einzelnen Symbole $\sigma \in \Sigma$ in einem Wort angeben.
+ ges.: präfixfreier (binärer) Code, sodass die Gesamtlänge des Codeworts möglichst gering ist.

---
*03.01.2023 Vorlesung `XVIII`*

Zeichenketten:
- endliche Folge von Symbolen aus einem Alphabet $\Sigma$

Code:
- eine Funktion $C: \Sigma \rightarrow \{0, 1\}^*$

Codierung von $w = w_1 w_2 \ldots w_n \mapsto C(w) = C(w_1) C(w_2) \ldots C(w_n)$

Ziel:
- finde einen Code $C$, der
  1. eindeutig ist
  2. $w$ möglichst kurz codiert

Ein Code ist präfixfrei, wenn kein Codewort ein Präfix eines anderes Codeworts ist.

+ präfixfreie Codes sind eindeutigdekodierbar
+ präfixfreier Code entspricht einem Binärbaum

Damit $C$ möglichst kurze Codewörter erstellt, müssen wir folgendes minimieren:

$$|C(w)| = \sum_{\sigma \in \Sigma} |C(\sigma)| \cdot h_\sigma$$

Idee: Konstruiere den Baum für $C$ gierig. [Huffman-Codierung](https://de.wikipedia.org/wiki/Huffman-Kodierung):

1. Lege Knoten für jedes Symbol and und annotiere jeden Knoten mit der entsprechenden Häufigkeiten.
2. Wähle die zwei Knoten mit den geringsten Häufigkeiten und vereinige diese zu einem Teilbaum. Annotiere die Summe der beiden Häufigkeiten an die Wurzel
3. Wiederhole 2. bis nur noch ein Baum/Wurzel/Knoten existiert.

---
*05.01.2023 Vorlesung `XIX`*

Datenkompression:
+ HC sind optimal, wenn nur zeichenweise codiert wird.
+ Andere methoden, die Längeneinheiten codieren z.B. RLE (Run-Length-Encoding) $1111110000000111 \rightarrow 673$
+ Lempel-Ziv-Welch und Varianten:
  + Erkenne wiederholende Wörtermuster
  + war patentier, Patent ist nun aber ausgelaufen
  + wird z.B. in GIF verwendet
+ Unterscheidung zwischen verlustfreien und verlustbehafteten Kompressionsverfahren. Bei verlustfreie Verfahren, erhält man nach der Dekompression wieder das Original
+ verlustbehaftete Verfahren sind bei medienKompression sehr verbreitet, z.B. Bildern (JPEG), Ton (MP4), Video (MPEG4). Hier werden nur wichtige Informationen gespeichert. Dies wird überwiegend mittels mathematischer Transformationen durchgeführt:
  + Fouriertransformation (FFT)
  + Discrete-Cosine-Transformation
  + Wavelet-Transformation

### Ähnlichkeit von Zeichenketten

Wie ähnlich sind zwei Zeichenketten? \
z.B. Unix-Befehl `diff`, `fc`, `cmp` \
Formal: Finde die längste gemeinsame Teilfolge
+ geg.: $s = s_1 \ldots s_n, t = t_1 \ldots t_l
+ ges.: $\{i_1, \ldots, i_k\}$ mit $i_1 < \ldots < i_k$ sodass $\{j_1, \ldots, j_k\}$ mit $j_1 < \ldots < j_k$ existiert: $s_{i_1} \ldots s_{i_k} = t_{j_1} \ldots t_{t_k}$ und $k$ maximal ist

Wie findet man LCS allgemein? Rekursion mit Dynamic Programming.\
Wir fangen zuerst damit an, die Länge der LCS zu berechnen.
+ Wenn beide Strings mit dem gleichen Symbol aufhören, dann füge es der LCS hinzu. Entferne das Symbol aus beiden String und gehe rekursiv vor.
+ Sonst, entferne das letzte Symbol von $s$ und gehe rekursiv vor.
+ oder entfern das letzte Symbol von $t$ und gehe rekursiv vor.

+ Die direkte Implementierung der Rekursion ist ineffizient; Lösung: Dynamic Programming
+ verwende Tabelle LLCS ($k+1 \times l+1$)
+ fülle LLCS sukzessive auf
+ Laufzeit ist in $\mathcal{O}(k \cdot l)$

---
*10.01.2023 Vorlesung `XX`*

### String-Suche
gegeben:
+ zwei Strings $s=s_1 \ldots s_n$ und $t=t_1 \ldots t_m$ mit $m \le n$

Frage:
+ Kommt $t$ in $s$ vor?

Naiver Algorithmus:
```python
def naiv(s, t):
    m = len(t)
    n = len(s)
    for i in range(n-m+1):
        j = 0
        while j < m and s[i+j] == t[j]:
            j += 1
        if j == m:
            return True
    return False
```

Rabin-Karp-Algorithmus
```python
def rabin_karp(s, t):
    m = len(t)
    n = len(s)
    for i in range(n-m+1):
        if h(s[i:i+m]) == h(t):
            if s[i:i+m] == t:
                return i
```

$$h(s) = \sum_{k=0}^j |\Sigma|^{j-k} \cdot |s_j|$$

---
*17.01.2023 Vorlesung `XXIII`*

### Tries, Digitale Suchbäume

Es gibt eine Variante von Tries:
+ komprimierte Tries/PATRICIA Tries

Idee: Spare Platz, indem Pfade mit nur einem Kind zu einer Kante zusammengefasst werden. Die Kante ist beschriftet mit dem Teilstring

Nur $\mathcal{O}(n)$ statt $\mathcal{O}(\sum_{s \in S}|s|)$ Knoten. Aber der Speicherbedarf ist immer noch $\mathcal{O}(\sum_{s \in S} |s|)$, da wir Labels speichern müssen.

PATRICIA steht für Practical Algorithm To Retrieve Information Coded In Alphanumeric.


## Exkurs Objektorientierte Programmierung (OOP)

Frage: Wie kann uns eine Programmiersprache dabei helfen, gute Software zu schreiben?

Gute Software ist:
+ fehlerfrei
+ robust
+ erweiterbar
+ verständlich
+ wartbar
+ effizient
+ modular
+ nützlich
+ interoperabel

Ursprüngliche imperative Programmiersprachen (FORTRAN, COBOL, BASIC,etc.) behandeln den Code als Folge von Anweisungen und der Kontrollfluss wurde durch Sprünge realisiert (GOTO).

Vorteile:
+ sehr maschinennah
+ erlaubt sehr effizienten & kompakten Code

Nachteile:
+ Spaghetti-Code
+ schwer zu verstehen
+ fehleranfällig

Lösung: Strukturierte Programmierung \
Der Code besteht aus Blöcken, die als Einheiten verstanden werden. Es gibt Kontrollstrukturen, die den Fluss zwischen den Blöcken bewegen. Sprünge sind nicht vorhanden.

Nächster Schritt: Modulare Programmierung \
Dort unterteilt man das Porgramm in möglichst unabhängige Einheiten (Module). Die Module kommunizieren über feste, klar spezifizierte Schnittstellen (Interface). Die Umsetzung der Schnittstelle bleibt nach Außen verborgen (Geheimnisprinzip).

---
*19.01.2023 Vorlesung `XXIV`*

Objekte entstehen aus Klassen indem man sie anlegt (instanziiert).

Dabei wird ein Konstruktor aufgerufen,der das Objekt initialisiert.

Zugriff auf Attribute & Methden des Objektes erfolgt über der `.`-Syntax

Methoden erhalten einen ersten Parameter, der das Objekt bezeichnet, auf dem die Methode aufgerufen wird. (In Python wird dieser Parameter explizit hingeschrieben, heißt typischerweise `self`)

Ein Objekt besitzt zwei Dinge:
+ Einen Zustand (Der Inhalt aller Attribute)
+ Eine Identität (entsteht beim Anlegen)

### Kapselung (Encapsulation)
Existiert nicht in Python. \
Objekte/Klassen können bestimmte Eigenschaften/Methoden nach außen verbergen. Das erlaubt eine Trennung von Interface und Implementierung und hilft uns Invarianten zu erhalten.

### Klassenvariablen & statische Methoden
Klassenvariablen sind Attribute,die von allen Objekten einer Klasse geteilt werden.

Man kann in Klassen auch Methoden definieren, die nur auf Klassenvariablen operieren (statische Methoden).

### Vererbung (Inheritance)
Ermöglicht Wiederverwendbarkeit, Erweiterbarkeit und Flexibilität im Code. Vererbung bedeutet, wir können aus einer bestehenden Klasse (Oberklasse) neue Klassen ableiten (Unterklasse). Die Unterklasse kann zusätzliche Attribute und Methoden hinzufügen oder bestehende Methoden abändern/überschreiben (overwrite).

Methoden die überschrieben werden können heißen virtuelle Methoden. In Python sind alle Methoden virtuelle Methoden.

Überall wo ein Objekt der Oberklasse erwartet wird, kann man auch ein Objekt der Unterklasse verwenden. (Inklusionspolymorphismus)

---
*24.01.2023 Vorlesung `XXV`*

## Graphen

Graphen sind mathematische Abstraktionen von Objekten & Beziehungen zwischen ihnen. Die Objekte werden Knoten und die Beziehungen Kanten genannt.

$$G = (V, E)$$

$V$ ist eine beliebige endliche Menge von Knoten \
$E$ ist eine Menge von Paaren von Knoten

Viele Dinge aus dem täglichen Leben lassen sich als Graphen modellieren, z.B.:
+ Verkehrsnetze
+ Stammbaum
+ Webgraph
+ soziale Graphen
+ Abhängigkeitsgraphen
+ Zustandgraphen mit Übergängen

Auf Graphen ergeben sich viel interessante Algorithmische Fragestellungen:
+ Zusammenhang
  + Geg.: zwei Knoten $s, t \in V$
  + Ges.: Kann ich $t$ von $s$ aus erreichen, wenn man einer Folge von Kanten folgt?
+ Kürzeste Verbindung
  + minimale Anzahl an Kanten zwischen zwei zusammenhängenden Knoten.
+ Was ist,wenn man Abstände berücksichtigt?

Wie stellen wir Graphen als Datenstruktur da?

Es gibt viele Möglichkeiten:
+ Adjazenzliste
+ Adjazenzmatrix
+ uvm.

#### Adjazenzmatrix
2-dimensionale Matrix (Array) der größe $|V| \times |V|$. D.h. es gibt eine Zeile/Spalte für jeden Knoten im Graphen. Der Eintrag $m_{i, j}$ ist 1, wenn es eine Kante zwischen $i$ und $j$ gibt, sonst 0.

#### Adjazenzliste
Für jeden Knoten $v \in V$ speichern wir die Liste der benachbarten Knoten.

---
*31.01.2023 Vorlesung `XVII`*

### Kürzeste Wege in Graphen

2 Varianten:
+ ungerichtete Graphen
+ gerichtete Graphen

Geg:
+ Graphen $G = (V, E)$
+ zwei Knoten $s, t \in V$

Ges:
+ Finde einen Pfad von $s$ nach $t$, der die minimale Anzahl an Kanten hat.

SPSP Problem steht für Single Pair Shortest Path. Es gibt auch ein allgemeineres Problem: SSSP (Single Source Shortest Path)

Die Algorithmen die wir betrachten, lösen das SSSP Problem. Das lässt sich im allgemeinen auch nicht vermeiden, denn es gilt die Teilpfadoptimalitätseigenschaft

Sei $\Pi: s \rightarrow u_1 \rightarrow \ldots \rightarrow u_n \rightarrow t$ der kürzeste Weg von $s$ nach $t$. Dann ist $s \rightarrow \ldots \rightarrow u_i$ der kürzeste Weg von $s$ nach $u_i$ für alle $i$

Es wäre im Allgemeinen zu ineffizient die kürzesten Wege zwischen $s$ und allen anderen Knoten explizit aufzulisten. Statt dessen berechnet man einen Kürzesten-Wege-Baum.

Darstellung im Algorithmus: Jeder Knoten $V$ besitzt eine Eigenschaft `v.pred`, welche den Vorgänger von $v$ auf kürzestem Weg von $s$ nach $v$ darstellt.

```python
def bfs(s):
    Q = []
    s.found = True
    s.d = 0
    s.pred = None
    Q.append(s)
    while len(Q) > 0:
        v = Q.pop(0)
        for e in v.outgoingEdges():
            w = e.opposite(v)
            if not w.found:
                w.found = True
                w.d = v.d + 1
                w.pred = v
                Q.append(w)
```

Diese `bfs` berehcnet einen Kürzesten-Wege-Baum für $s$ in $G$ und die `v.d` Eigenschaften geben die kürzesten Abstände wieder.

Wie löst man das SSSP Problem in einem gewichteten Graphen. Jede Kante besitzt eine Länge `e.length`. Wir suchen Wege, welche die Summe der Kantenlängen minimieren.

Idee: Benutze statt der Warteschlange ein Prioritätswarteschlange.

```python
def bfs(s):
    Q = PrioQueue()
    s.found = True
    s.d = 0
    s.pred = None
    Q.insert(s, 0)
    while len(Q) > 0:
        v = Q.extractMin()
        for e in v.outgoingEdges():
            w = e.opposite(v)
            if not w.found or v.d + e.length < w.d:
                w.found = True
                w.d = v.d + e.length
                w.pred = v
                if w in Q:
                    Q.decreaseKey(w, w.d)
                else:
                    Q.insert(w, w.d)
```

---
*02.02.2023 Vorlesung `XXVIII`*

Satz: \
Wenn `e.length` $\ge 0$ ist, für alle $e \in E$, dann findet Dijkstra's Algorithmus einen korrekten Kürzeste-Wege-Baum für $G$ und die Eigenschaften `v.d` geben die Abstände zwischen $s$ und $v$für alle $v \in V$ an.

Die Laufzeit des Dijkstra Algorithmuses ist in $\mathcal{O}(|V| \cdot |E| \cdot \log(|V|))$.
